package com.taskaprosthetics.trainingtest

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.View
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * What does this do? Dunno...
 */
class MyGameView@JvmOverloads constructor(context:Context,attrs:AttributeSet?=null,defStyle:Int=0,defStyleRes:Int=0):View(context,attrs,defStyle,defStyleRes){

    var moving = true

    private val squareSize = 50
    private var squareLeft = width

    init {
        //TODO: get them to do something with coroutines
        GlobalScope.launch {
            while(moving){
                if(moving){
                    squareLeft -= 100
                    if(squareLeft < 0)
                        squareLeft = width
                }

                Thread.sleep(100)
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

                val squareTop = (height-squareSize)/2

                    val squarePaint = Paint().apply {
                        color = Color.BLACK
                    }

                    val square = Rect(
                        squareLeft, squareTop, squareLeft+squareSize, squareTop+squareSize
                    )

                    canvas.drawRect(square, squarePaint)

invalidate()
requestLayout()
forceLayout()
}
}